package com.androiddevlivehangout.mydevelopernotes.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.androiddevlivehangout.mydevelopernotes.R;
import com.androiddevlivehangout.mydevelopernotes.api.response.NoteResponse;

import java.util.List;

/**
 * NotesAdapter class
 */
public class NotesAdapter extends BaseAdapter {

    private List<NoteResponse> data;
    private Context context;

    public NotesAdapter(List<NoteResponse> data, Context context) {
        this.data = data;
        this.context = context;
    }

    public void setData(List<NoteResponse> data) {
        this.data = data;
    }

    /**
     * @see android.widget.BaseAdapter#getCount()
     */
    @Override
    public int getCount() {
        return data.size();
    }

    /**
     * @see android.widget.BaseAdapter#getItem(int)
     */
    @Override
    public NoteResponse getItem(int position) {
        return data.get(position);
    }

    /**
     * @see android.widget.BaseAdapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class PlaceHolder {
        TextView tvTitle;
        TextView tvDescription;
        LinearLayout llContentTags;

        public static PlaceHolder generate(View convertView) {
            PlaceHolder placeHolder = new PlaceHolder();
            placeHolder.tvTitle = (TextView) convertView.findViewById(R.id.note_row_tv_title);
            placeHolder.tvDescription = (TextView) convertView.findViewById(R.id.note_row_tv_description);
            placeHolder.llContentTags = (LinearLayout) convertView.findViewById(R.id.note_row_ll_content_tags);
            return placeHolder;
        }

    }

    /**
     * @see android.widget.BaseAdapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NoteResponse item = data.get(position);
        PlaceHolder placeHolder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.note_row, null);
            placeHolder = PlaceHolder.generate(convertView);
            convertView.setTag(placeHolder);
        } else {
            placeHolder = (PlaceHolder) convertView.getTag();
        }

        placeHolder.tvTitle.setText(item.getTitle());
        placeHolder.tvDescription.setText(item.getDescription());
        placeHolder.llContentTags.removeAllViews();
        if (item.getTagList().size() > 0) {
            for (String tag : item.getTagList()) {
                View view = View.inflate(context, R.layout.note_row_tag, null);
                ((TextView)view.findViewById(R.id.note_row_tag_tv_type)).setText(tag);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.rightMargin = (int) context.getResources().getDimension(R.dimen.margin_default);
                placeHolder.llContentTags.addView(view, layoutParams);
            }
        }

        return convertView;
    }
}
