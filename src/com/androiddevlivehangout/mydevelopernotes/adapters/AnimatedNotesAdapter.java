package com.androiddevlivehangout.mydevelopernotes.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import com.androiddevlivehangout.mydevelopernotes.api.response.NoteResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: AnderWeb
 * Date: 2/1/13
 * Time: 12:08 PM
 */
public class AnimatedNotesAdapter extends NotesAdapter {

    private int lastAnimatedPosition = -1;

    public AnimatedNotesAdapter(List<NoteResponse> data, Context context) {
        super(data, context);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        if (position>lastAnimatedPosition) {
            v.setRotationX(80);
            v.setTranslationY(400);
            v.animate()
                    .rotationX(0)
                    .translationY(0)
                    .setDuration(600)
                    .setStartDelay(150)
                    .setInterpolator(new DecelerateInterpolator());
            lastAnimatedPosition = position;
        }
        return v;
    }
}
