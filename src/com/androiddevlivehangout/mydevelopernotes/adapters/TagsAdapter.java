package com.androiddevlivehangout.mydevelopernotes.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.androiddevlivehangout.mydevelopernotes.R;
import com.androiddevlivehangout.mydevelopernotes.api.response.TagResponse;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AnderWeb
 * Date: 2/5/13
 * Time: 12:19 PM
 */
public class TagsAdapter extends ArrayAdapter<TagResponse> {

    public TagsAdapter(Context context, List<TagResponse> objects) {
        super(context, R.layout.tag_row, objects);
    }

    private static class TagHolder {
        TextView tvName;

        public static TagHolder generate(View convertView) {
            TagHolder tagHolder = new TagHolder();
            tagHolder.tvName = (TextView) convertView.findViewById(R.id.tag_row_tv_name);
            return tagHolder;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);

        //TODO: Tag's color stuff

        return convertView;
    }
}
