package com.androiddevlivehangout.mydevelopernotes.activities;

import android.app.Activity;
import android.os.Bundle;
import com.androiddevlivehangout.mydevelopernotes.utils.PreferencesManager;

/**
 * BaseActivity class
 */
public class BaseActivity extends Activity {

    /**
     *
     * @see Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferencesManager.getInstance(this).setCurrentTheme(this);
    }

}
