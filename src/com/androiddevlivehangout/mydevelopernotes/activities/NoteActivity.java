package com.androiddevlivehangout.mydevelopernotes.activities;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androiddevlivehangout.mydevelopernotes.R;
import com.androiddevlivehangout.mydevelopernotes.api.response.NoteResponse;
import com.androiddevlivehangout.mydevelopernotes.dialogs.AddTagDialog;
import com.androiddevlivehangout.mydevelopernotes.dialogs.EditTagDialog;
import com.androiddevlivehangout.mydevelopernotes.dialogs.TagListener;
import com.androiddevlivehangout.mydevelopernotes.managers.NotesManager;

/**
 * NoteActivity class
 */
public class NoteActivity extends BaseActivity {

    public static final String NOTE_OBJECT = "NOTE_OBJECT";
    private NoteResponse currentNote;
    private EditText etTitle;
    private EditText etDescription;
    private RatingBar rbPriority;
    private LinearLayout llContentTags;
    private ImageButton ibAddTag;

    /**
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_activity);

        ActionBar ab = getActionBar();
        ab.setDisplayShowHomeEnabled(false);
        ab.setDisplayShowTitleEnabled(false);
        final LayoutInflater inflater = (LayoutInflater)getSystemService("layout_inflater");
        View view = inflater.inflate(R.layout.action_bar_edit_mode,null);

        view.findViewById(R.id.action_bar_button_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNote();
            }
        });

        view.findViewById(R.id.action_bar_button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelNote();
            }
        });

        ab.setCustomView(view);
        ab.setDisplayShowCustomEnabled(true);

        etTitle = (EditText) findViewById(R.id.note_et_title);
        etDescription = (EditText) findViewById(R.id.note_et_description);
        rbPriority = (RatingBar) findViewById(R.id.note_rb_priority);
        rbPriority.setMax(getResources().getInteger(R.integer.maxNotePriority));
        llContentTags = (LinearLayout) findViewById(R.id.note_ll_content_tags);
        ibAddTag = (ImageButton) findViewById(R.id.note_ib_add_tags);

        ibAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddTagDialog addTagDialog = new AddTagDialog(new TagListener() {
                    @Override
                    public void addTag(String tag) {
                        currentNote.addTag(tag);
                        loadTags();
                    }

                    @Override
                    public void removeTag(String tag) {
                    }

                    @Override
                    public void editTag(String tag, String newTag) {
                    }
                });
                addTagDialog.show(getFragmentManager(), "dialogs");
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(NOTE_OBJECT) && extras.getSerializable(NOTE_OBJECT) instanceof NoteResponse) {
            currentNote = (NoteResponse)extras.getSerializable(NOTE_OBJECT);
            loadNote();
        } else {
            currentNote = new NoteResponse();
            loadTags();
        }
    }

    private void cancelNote() {
        finish();
    }

    private void saveNote() {
        currentNote.setTitle(etTitle.getText().toString());
        currentNote.setDescription(etDescription.getText().toString());
        currentNote.setPriority((int) rbPriority.getRating());
        NotesManager.getInstance().saveNote(currentNote);
        setResult(RESULT_OK);
        finish();
    }

    private void loadNote() {
        etTitle.setText(currentNote.getTitle());
        etDescription.setText(currentNote.getDescription());
        rbPriority.setRating(currentNote.getPriority());
        loadTags();
    }

    private void loadTags() {
        llContentTags.removeAllViews();
        if (currentNote.getTagList().size() > 0) {
            for (final String tag : currentNote.getTagList()) {
                View view = View.inflate(this, R.layout.note_row_tag, null);
                ((TextView)view.findViewById(R.id.note_row_tag_tv_type)).setText(tag);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_default);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditTagDialog editTagDialog = new EditTagDialog(new TagListener() {
                            @Override
                            public void addTag(String tag) {
                            }

                            @Override
                            public void removeTag(String tag) {
                                currentNote.removeTag(tag);
                                loadTags();
                            }

                            @Override
                            public void editTag(String tag, String newTag) {
                                currentNote.replaceTag(tag, newTag);
                                loadTags();
                            }
                        }, tag);
                        editTagDialog.show(getFragmentManager(), "dialogs");
                    }
                });
                llContentTags.addView(view, layoutParams);
            }
        } else {
            View view = View.inflate(this, R.layout.note_row_tag, null);
            ((TextView)view.findViewById(R.id.note_row_tag_tv_type)).setText(getString(R.string.addTagMessage));
            llContentTags.addView(view);
        }
    }


}
