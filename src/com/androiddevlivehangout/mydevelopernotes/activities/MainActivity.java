package com.androiddevlivehangout.mydevelopernotes.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.androiddevlivehangout.mydevelopernotes.R;
import com.androiddevlivehangout.mydevelopernotes.adapters.AnimatedNotesAdapter;
import com.androiddevlivehangout.mydevelopernotes.adapters.NotesAdapter;
import com.androiddevlivehangout.mydevelopernotes.adapters.TagsAdapter;
import com.androiddevlivehangout.mydevelopernotes.api.response.TagResponse;
import com.androiddevlivehangout.mydevelopernotes.components.ui.widgets.SlideMenu;
import com.androiddevlivehangout.mydevelopernotes.managers.NotesManager;
import com.androiddevlivehangout.mydevelopernotes.utils.FakeDataUtils;
import com.androiddevlivehangout.mydevelopernotes.utils.PreferencesManager;

/**
 * MainActivity class
 */
public class MainActivity extends BaseActivity {

    private static final int REQUEST_CODE_NOTE_ACTIVITY = 0;

    private ListView lvNotes;
    private ListView lvTags;
    private SlideMenu slideMenu;
    private NotesAdapter notesAdapter;
    private TagsAdapter tagsAdapter;

    /**
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        FakeDataUtils.loadTestData(); //TODO: Remove this line when the database is ready

        slideMenu = (SlideMenu) findViewById(R.id.slide_menu);

        final NotesManager manager = NotesManager.getInstance();

        lvNotes = (ListView) findViewById(R.id.main_lv_list_notes);
        notesAdapter = new AnimatedNotesAdapter(manager.getNotesListWithActiveTags(), this);
        lvNotes.setAdapter(notesAdapter);
        lvNotes.setOnItemClickListener(onItemClickListView);

        lvTags = (ListView) findViewById(R.id.main_lv_list_tags);
        tagsAdapter = new TagsAdapter(this, manager.getTagsList());
        lvTags.setAdapter(tagsAdapter);

        markCurrentTagsAsChecked();

        lvTags.setOnItemClickListener(onItemClickTagsListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_notes, menu);
        return true;
    }

    /**
     * Click on ListView
     */
    private final AdapterView.OnItemClickListener onItemClickListView = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(MainActivity.this, NoteActivity.class);
            intent.putExtra(NoteActivity.NOTE_OBJECT, notesAdapter.getItem(position));
            startActivityForResult(intent, REQUEST_CODE_NOTE_ACTIVITY);
        }
    };

    /**
     * Click on Tags ListView
     */
    private final AdapterView.OnItemClickListener onItemClickTagsListView = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            boolean itemChecked = lvTags.isItemChecked(position);
            tagsAdapter.getItem(position).setActive(itemChecked);
            updateTagFilter(tagsAdapter.getItem(position));
        }
    };

    /**
     * @see Activity#onMenuItemSelected(int, android.view.MenuItem)
     */
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case android.R.id.home: //Actionbar home/up icon
                slideMenu.toggle();
                handled = true;
                break;
            case R.id.menu_save: //New note click
                Intent intent = new Intent(MainActivity.this, NoteActivity.class);
                startActivityForResult(intent, REQUEST_CODE_NOTE_ACTIVITY);
                handled = true;
                break;
            case R.id.menu_change_theme: // Change Theme
                PreferencesManager.getInstance(this).swapTheme(this);
                recreate();
                break;
            default:
                handled = super.onMenuItemSelected(featureId, item);
                break;
        }
        return handled;
    }

    @Override
    public void onBackPressed() {
        if (slideMenu.isMenuOpen()) {
            slideMenu.toggle();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_NOTE_ACTIVITY:
                if (resultCode == Activity.RESULT_OK) {
                    notesAdapter.notifyDataSetChanged();
                }
        }
    }

    private void markCurrentTagsAsChecked() {
        //TODO: Replace with saved tags Visible from the user

        final int tagsCount = tagsAdapter.getCount();
        for (int i = 0; i < tagsCount; i++) {
            lvTags.setItemChecked(i, true);
        }
    }

    /**
     * Update notes filtered
     *
     * @param tag changed tag
     */
    private void updateTagFilter(TagResponse tag) {
        NotesManager.getInstance().updateTagState(tag);
        notesAdapter.setData(NotesManager.getInstance().getNotesListWithActiveTags());
        notesAdapter.notifyDataSetChanged();
    }

}
