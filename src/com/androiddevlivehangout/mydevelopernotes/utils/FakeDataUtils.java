package com.androiddevlivehangout.mydevelopernotes.utils;

import com.androiddevlivehangout.mydevelopernotes.api.response.NoteResponse;
import com.androiddevlivehangout.mydevelopernotes.managers.NotesManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//TODO: Delete Class when database is ready
public class FakeDataUtils {

    private static String[] types =
            {
                    "Java",
                    "Android",
                    "iOS",
                    "C++",
                    ".NET",
                    "Python",
                    "PHP"
            };

    private static String[] titles =
            {
                    "Henry IV (1)",
                    "Henry V",
                    "Henry VIII",
                    "Richard II",
                    "Richard III",
                    "Merchant of Venice",
                    "Othello",
                    "King Lear"
            };

    private static String[] quoteList =
            {
                    "So shaken as we are, so wan with care," +
                            "Find we a time for frighted peace to pant," +
                            "And breathe short-winded accents of new broils.",

                    "Hear him but reason in divinity," +
                            "And all-admiring with an inward wish" +
                            "You would desire the king were made a prelate.",

                    "I come no more to make you laugh: things now," +
                            "That bear a weighty and a serious brow," +
                            "Sad, high, and working, full of state and woe," +
                            "Such noble scenes as draw the eye to flow.",

                    "First, heaven be the record to my speech!" +
                            "In the devotion of a subject's love," +
                            "Tendering the precious safety of my prince," +
                            "And free from other misbegotten hate.",

                    "Now is the winter of our discontent" +
                            "Made glorious summer by this sun of York;" +
                            "And all the clouds that lour'd upon our house" +
                            "In the deep bosom of the ocean buried",

                    "To bait fish withal: if it will feed nothing else," +
                            "it will feed my revenge. He hath disgraced me, and" +
                            "hindered me half a million; laughed at my losses.",

                    "Virtue! a fig! 'tis in ourselves that we are thus" +
                            "or thus. Our bodies are our gardens, to the which" +
                            "our wills are gardeners.",

                    "Blow, winds, and crack your cheeks! rage! blow!" +
                            "You cataracts and hurricanoes, spout" +
                            "Till you have drench'd our steeples, drown'd the cocks!"
            };

    private static final int NUM_FAKE_NOTES = 20;

    /**
     * Create test data
     */
    public static void loadTestData() {
        List<NoteResponse> notesList = new ArrayList<NoteResponse>();
        for (int i = 0; i < NUM_FAKE_NOTES; i++) {
            NoteResponse noteResponse1 = new NoteResponse();
            noteResponse1.setId(String.valueOf(i));
            noteResponse1.setTitle(generateTitle());
            noteResponse1.setDescription(generatePhrase());
            noteResponse1.setTagListFromString(generateTypes());
            noteResponse1.setRead(false);
            noteResponse1.setPriority(2);
            NotesManager.getInstance().saveNote(noteResponse1);
        }

    }

    public static String generateTitle() {
        Random random = new Random();
        return titles[random.nextInt(quoteList.length)];
    }

    public static String generatePhrase() {
        int numberOfWords = new Random().nextInt(3) + 1;
        String randomString = "";
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            randomString += quoteList[random.nextInt(quoteList.length)];
        }
        return randomString;
    }

    public static String generateTypes() {
        int numberOfWords = new Random().nextInt(3) + 1;
        String randomString = "";
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            if (i != 0) randomString += ",";
            randomString += types[random.nextInt(types.length)];
        }
        return randomString;
    }

}
