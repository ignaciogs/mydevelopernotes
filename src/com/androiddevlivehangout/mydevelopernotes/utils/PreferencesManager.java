package com.androiddevlivehangout.mydevelopernotes.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.androiddevlivehangout.mydevelopernotes.R;

public class PreferencesManager {

    public static final int THEME_LIGHT = 0;
    public static final int THEME_DARK = 1;

    public static final String SHARED_PREFERENCES_NAME = "MY_DEVELOPER_NOTES_PREFERENCES";

    /**
     * Instance
     */
    private static PreferencesManager preferencesManager = null;

    /**
     * Shared Preferences
     */
    private SharedPreferences sharedPreferences;

    /**
     * Preferences variables
     */
    private static final String SHARED_PREFERENCES_THEME = "SHARED_PREFERENCES_THEME";

    /**
     * Constructor
     *
     * @param context
     */
    private PreferencesManager(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static PreferencesManager getInstance(Context context) {
        if (preferencesManager == null) {
            preferencesManager = new PreferencesManager(context);
        }
        return preferencesManager;
    }


    public void setTheme(int theme) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SHARED_PREFERENCES_THEME, theme);
        editor.commit();
    }

    public int getTheme() {
        return sharedPreferences.getInt(SHARED_PREFERENCES_THEME, THEME_DARK);
    }

    public void setCurrentTheme(Context context) {
        if (getTheme() == THEME_DARK) {
            context.setTheme(R.style.Theme_MyDev_Dark);
        } else {
            context.setTheme(R.style.Theme_MyDev_Light);
        }
    }

    public void swapTheme(Context context) {
        if (getTheme() == THEME_DARK) {
            setTheme(THEME_LIGHT);
            context.setTheme(R.style.Theme_MyDev_Light);
        } else {
            setTheme(THEME_DARK);
            context.setTheme(R.style.Theme_MyDev_Dark);
        }
    }

}
