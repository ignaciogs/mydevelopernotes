/*
 *
 *  * Copyright (C) 2012 QThru.com
 *  * http://www.qthru.com/
 *  * All rights reserved
 *  * Developed for QThru.com by:
 *  *
 *  * 47 Degrees, LLC
 *  * http://47deg.com
 *  * hello@47deg.com
 *
 */

package com.androiddevlivehangout.mydevelopernotes.dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;
import com.androiddevlivehangout.mydevelopernotes.R;

public class EditTagDialog extends DialogFragment {

    private TagListener tagListener;
    private String tag;

    public EditTagDialog(TagListener tagListener, String tag) {
        this.tagListener = tagListener;
        this.tag = tag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText view = new EditText(getActivity());
        view.setText(tag);

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.editTag)
                .setView(view)
                .setPositiveButton(R.string.edit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tagListener.editTag(tag, view.getText().toString());
                    }
                })
                .setNeutralButton(R.string.remove, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tagListener.removeTag(tag);
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();

    }
}

