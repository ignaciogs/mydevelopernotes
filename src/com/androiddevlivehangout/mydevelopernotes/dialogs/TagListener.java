package com.androiddevlivehangout.mydevelopernotes.dialogs;

public interface TagListener {

    void addTag(String tag);
    void removeTag(String tag);
    void editTag(String tag, String newTag);

}
