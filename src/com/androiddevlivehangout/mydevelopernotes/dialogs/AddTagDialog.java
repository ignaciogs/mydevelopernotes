/*
 *
 *  * Copyright (C) 2012 QThru.com
 *  * http://www.qthru.com/
 *  * All rights reserved
 *  * Developed for QThru.com by:
 *  *
 *  * 47 Degrees, LLC
 *  * http://47deg.com
 *  * hello@47deg.com
 *
 */

package com.androiddevlivehangout.mydevelopernotes.dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;
import com.androiddevlivehangout.mydevelopernotes.R;

public class AddTagDialog extends DialogFragment {

    private TagListener tagListener;

    public AddTagDialog(TagListener tagListener) {
        this.tagListener = tagListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText view = new EditText(getActivity());

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.addTag)
                .setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tagListener.addTag(view.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();

    }
}

