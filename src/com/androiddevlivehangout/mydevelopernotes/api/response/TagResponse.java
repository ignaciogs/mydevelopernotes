package com.androiddevlivehangout.mydevelopernotes.api.response;

import java.io.Serializable;

/**
 * TagResponse class
 */
public class TagResponse implements Serializable {

    private String name;

    private int color;

    private boolean active = false;

    public TagResponse(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    public TagResponse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @see Class#equals(Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TagResponse that = (TagResponse) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * @see Class#hashCode()
     */
    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
