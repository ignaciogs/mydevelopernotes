package com.androiddevlivehangout.mydevelopernotes.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * NoteResponse class
 */
public class NoteResponse implements Serializable {

    private String id;

    private String title;

    private String description;

    private String link;

    private boolean read;

    private int priority;

    private List<String> tagsList = new ArrayList<String>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<String> getTagList() {
        return tagsList;
    }

    public void setTagList(List<String> tagsList) {
        this.tagsList = tagsList;
    }

    /**
     * Set Tags List from a String object separated by commas
     *
     * @param data String object with tags
     */
    public void setTagListFromString(String data) {
        data = data.replaceAll(" ", "");
        String[] tags = data.split(",");
        if (tagsList == null) {
            tagsList = new ArrayList<String>();
        } else {
            tagsList.clear();
        }
        for (String tag : tags) {
            tagsList.add(tag);
        }
    }

    /**
     * Get a String with tags list
     *
     * @return String object with the tags list
     */
    public String getTagsListInString() {
        StringBuilder stringBuilder = new StringBuilder();
        String loopDelim = "";
        for (String string : tagsList) {
            stringBuilder.append(loopDelim);
            stringBuilder.append(string);
            loopDelim = ",";
        }
        return stringBuilder.toString();
    }

    public void addTag(String tag) {
        tagsList.add(tag);
    }

    public void removeTag(String tag) {
        tagsList.remove(tag);
    }

    public void replaceTag(String tag, String newTag) {
        tagsList.remove(tag);
        tagsList.add(newTag);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NoteResponse that = (NoteResponse) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
