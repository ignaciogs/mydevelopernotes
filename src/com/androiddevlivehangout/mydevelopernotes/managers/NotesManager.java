package com.androiddevlivehangout.mydevelopernotes.managers;

import com.androiddevlivehangout.mydevelopernotes.api.response.NoteResponse;
import com.androiddevlivehangout.mydevelopernotes.api.response.TagResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * NotesManager class
 */
public class NotesManager {

    private static NotesManager INSTANCE = null;
    private List<NoteResponse> notes;
    private List<TagResponse> tags;

    private NotesManager() {
        notes = new ArrayList<NoteResponse>();
        tags = new ArrayList<TagResponse>();
    }

    private synchronized static void createInstance() {
        INSTANCE = new NotesManager();
    }

    public static NotesManager getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }

    /**
     * Return the notes list
     *
     * @param args Tag's list to filter
     * @return Notes list
     */
    public List<NoteResponse> getNotesList(TagResponse... args) {
        List<NoteResponse> result = new ArrayList<NoteResponse>();
        for (NoteResponse note : notes) {
            for (TagResponse tag : args) {
                if (note.getTagList().contains(tag.getName())) {
                    result.add(note);
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Get all notes with tags actives
     *
     * @return Notes list
     */
    public List<NoteResponse> getNotesListWithActiveTags() {
        List<TagResponse> activeList = new ArrayList<TagResponse>();
        for (TagResponse tag : tags) {
            if (tag.isActive()) {
                activeList.add(tag);
            }
        }
        TagResponse[] list = activeList.toArray(new TagResponse[activeList.size()]);
        return getNotesList(list);
    }

    /**
     * Get current tags list
     *
     * @return Tags list
     */
    public List<TagResponse> getTagsList() {
        return tags;
    }

    /**
     * Store a note
     *
     * @param note Note to store
     */
    public void saveNote(NoteResponse note) {
        if (notes.contains(note)) { //Update Note
            int pos = notes.indexOf(note);
            notes.set(pos, note);
        } else { //Insert note
            note.setId(String.valueOf(notes.size()));
            notes.add(note);
        }
        updateTagsListFromNote(note);
    }

    /**
     * Update the tags list
     */
    private void updateTagsListFromNote(NoteResponse note) {
        for (String tag : note.getTagList()) {
            TagResponse tagResponse = new TagResponse(tag);
            if (!tags.contains(tagResponse)) {
                tagResponse.setActive(true);
                tags.add(tagResponse);
            }
        }
    }

    /**
     * Update state tag received
     *
     * @param tagResponse
     */
    public void updateTagState(TagResponse tagResponse) {
        int indexTag = tags.indexOf(tagResponse);
        if (indexTag > -1) {
            tags.get(indexTag).setActive(tagResponse.isActive());
        }
    }
}
